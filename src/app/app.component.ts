import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`.full-width { height: 100%; }`]
})
export class AppComponent { }
