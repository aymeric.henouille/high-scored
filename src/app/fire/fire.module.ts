import { NgModule } from '@angular/core';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

const modules = [
  AngularFireAuthModule,
  AngularFirestoreModule
];

@NgModule({
  imports: [ modules ],
  exports: [ modules ]
})
export class FireModule { }
