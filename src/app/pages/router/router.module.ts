import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from '../home/home-page/home-page.component';
import { AuthPageComponent } from '../auth/auth-page/auth-page.component';

const routes: Routes = [
  { path: 'home', component: HomePageComponent, loadChildren: () => import('../home/home.module').then(m => m.HomeModule) },
  { path: 'login', component: AuthPageComponent, loadChildren: () => import('../auth/auth.module').then(m => m.AuthModule) },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class RoutingModule { }
