import { NgModule } from '@angular/core';
import { RoutingModule } from './router/router.module';

const pages = [
  RoutingModule
];

@NgModule({
  imports: [ pages ],
  exports: [ pages ]
})
export class PagesModule { }
